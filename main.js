/*jshint esversion: 6 */
const RECOMMENDATIONS_MIN_RATINGS = 3;
const MIN_COMMON_MOVIES = 3;
const MIN_PCC = 0.7;
const MAX_STRONG_POSITIVE_USERS = 5;

function init() {
    sessionStorage.setItem('user-0', JSON.stringify(Array.from(new Map().entries())));
    const searchBar = document.getElementById('topnav-search');
    searchBar.value = '';
    initMovieDetails();
    getRecommendations();
    handleSearchLabel();
}

function initMovieDetails() {
    const p = document.getElementById('movie-details-txt');
    p.textContent = "Here will appear the details of a selected movie. Select a movie from your search results or the recommendation list.";
}

function keypressSearchButton(event) {
    if (event.keyCode === 13) {
        const text = document.getElementById('topnav-search').value;
        if (!text || text.trim() === '' || !text.match(/[a-zA-Z0-9\-:!?#()]{3,}/)) {
            console.log(`Invalid search keyword: "${text}"`)
        } else {
            doSearch();
        }
    }
}

function doSearch() {
    const text = document.getElementById('topnav-search').value;

    const body = `{"keyword": "${text}"}`;

    callAPI('post', '/movie', body)
        .then(resp => JSON.parse(resp))
        .then(resp => renderMovieTable(resp, 'tbl-previews'));

    document.getElementById('div-previews').hidden = false;
}

function renderMovieTable(movies, tableId) {
    const table = document.getElementById(tableId);
    if (movies && movies.length > 0) {
        clearTable(tableId);
        for (let m in movies) {
            let movie = parseMovie(movies[m]);
            let title = movie.title;
            if (movie.release) {
                title += `(${movie.release})`
            }

            let tr = document.createElement('tr');
            tr.textContent = title;
            tr.className = 'tr-preview';
            tr.addEventListener('click', function () {
                renderMovieDetails(movie);
            });
            table.appendChild(tr);
        }
    } else {
        const tr = document.createElement('tr');
        tr.textContent = 'Your search returned no results. Please try again.';
        clearTable(tableId, tr);
    }
}

function renderMovieDetails(movie) {
    const p = document.getElementById("movie-details-txt");
    p.textContent = null;
    document.getElementById('id-movie-id-td').value = movie.movieId;
    const myRating = getMyRatings().get(movie.movieId);

    let movieTitleTd = document.getElementById('id-movie-title-td');
    movieTitleTd.textContent = movie.title;
    let movieYearTd = document.getElementById('id-movie-year-td');
    movieYearTd.textContent = movie.release;
    let movieGenresTd = document.getElementById('id-movie-genres-td');
    movieGenresTd.textContent = movie.genres;
    let movieDiv = document.getElementById('div-movie');
    movieDiv.hidden = false;
    let ratingInput = document.getElementById('id-rating-input');
    ratingInput.value = myRating ? myRating : '0.5';
    let ratingPreview = document.getElementById('id-rating-preview');
    ratingPreview.value = myRating ? `${myRating} / 5` : '0.5 / 5';
}

function parseMovie(movie) {
    let movieTitle;
    let movieRelease;
    let movieGenres;
    let isEnclosedInQuotes = movie.title.startsWith('"') && movie.genres.endsWith('"');
    if (isEnclosedInQuotes) {
        let movieReleaseStr = movie.genres.match(/\([1-2][0-9]{3}\)/g);
        let titleFromGenres = movie.genres.replace('"', '').replace(/\(\d{4}\)/, '');
        let titleFromTitle = movie.title.replace('"', '');
        movieTitle = titleFromGenres.startsWith(' The') ?
            titleFromGenres.replace(',', '') + titleFromTitle : titleFromTitle + ', ' + titleFromGenres;
        movieRelease = movieReleaseStr[0].replace('(', '').replace(')', '');
        movieGenres = '-';
    } else {
        let movieReleaseStr = movie.title.match(/\([1-2][0-9]{3}\)/g);
        if (movieReleaseStr != undefined) {
            movieRelease = movieReleaseStr[0].replace('(', '').replace(')', '');
            movieTitle = movie.title.replaceAll(movieReleaseStr, '');
        } else {
            movieTitle = movie.title;
            movieRelease = '-';
        }
        movieGenres = movie.genres.replaceAll('|', ', ');
    }
    return new Movie(movie.movieId, movieTitle, movieGenres, movieRelease);
}

function rateMovie(movieId, rating) {
    movieId = movieId ? movieId : document.getElementById('id-movie-id-td').value;
    rating = rating ? rating : document.getElementById('id-rating-input').value;
    let movieRatings = new Map(JSON.parse(sessionStorage['user-0']));
    movieRatings.set(movieId, rating);
    sessionStorage.setItem('user-0', JSON.stringify(Array.from(movieRatings.entries())));
}

function getMyRatings() {
    return sessionStorage['user-0'] ? new Map(JSON.parse(sessionStorage['user-0'])) : new Map();
}

// for testing purposes
async function getRatingsNOW() {
    // lotr test
    // let start, end;
    // rateMovie(4993, 5);
    // rateMovie(5952, 5);
    // rateMovie(7143, 5);
    // await callAPI('post', '/ratings', "{\"movieList\": [4993, 5952, 7153]}")
    //     .then(r => JSON.parse(r))
    //     .then(movieRatings => {
    //         start = performance.now();
    //         findCorrelatedUsers(movieRatings);
    //         end = performance.now();
    //         console.log(`[!] Elapsed time = ${end-start}ms`);
    //         return movieRatings;
    //     });
}

/* Here be dragons... */
function findCorrelatedUsers(movieRatings) {
    let populatePearsonMap = function (ratingsMapInput) {
        for (const [userId, ratings] of ratingsMapInput) {
            if (ratings.size < MIN_COMMON_MOVIES) {
                ratingsMapInput.delete(userId);
            } else {
                pearsonMap.set(userId, calculatePearson(myRatings, ratings));
                ratingsMapInput.delete(userId);
            }
        }
    }
    let ratingsMap = new Map();
    movieRatings.forEach(mratings => {
        mratings.forEach(mr => {
            if (ratingsMap.has(mr.userId)) {
                let ratings = ratingsMap.get(mr.userId);
                ratings.set(mr.movieId, mr.rating);
                ratingsMap.set(mr.userId, ratings);
            } else {
                ratingsMap.set(mr.userId, new Map([[mr.movieId, mr.rating]]));
            }
        });
    });
    const myRatings = getMyRatings();
    // const myRatings = new Map();
    // myRatings.set(2571, 4.0);
    // myRatings.set(6365, 3.0);
    // myRatings.set(6934, 4.0);
    const pearsonMap = new Map();
    let start = performance.now();
    let first = 0;
    let last = ratingsMap.size > 500 ? 500 : ratingsMap.size;
    let strongPositivesCount = 0;
    do {
        let input = new Map(Array.from(ratingsMap).slice(first, last));
        populatePearsonMap(input);
        first = last;
        last = ratingsMap.size > last + 500 ? last + 500 : ratingsMap.size;
        if (ratingsMap.size === last) {
            break;
        }
        strongPositivesCount = findStrongPositives(pearsonMap);
    } while (strongPositivesCount < MAX_STRONG_POSITIVE_USERS);
    let end = performance.now();
    console.log(`Time to create person map [userId, pearson] = ${end - start}ms for ${pearsonMap.size} users`);
    let sortedPearsonMap = new Map([...pearsonMap.entries()].sort((a, b) => b[1] - a[1]));
    let countOfEligibleUsers = Array.from(sortedPearsonMap.values())
        .filter(p => p > MIN_PCC)
        .length;
    if (countOfEligibleUsers === 0){
        return [];
    }

    let tmp = Array.from(sortedPearsonMap).slice(0, 5); //TODO error prone
    sortedPearsonMap = new Map(tmp);

    return Array.from(sortedPearsonMap.keys());
}

function findStrongPositives(pearsonMap) {
    let count = 0;
    for (const p of pearsonMap.values()) {
        if (p > 0.85) {
            count++;
        }
    }
    return count;
}

function getRecommendations() {
    const myRatings = getMyRatings();
    const tableId = 'id-recommendations-tbl';
    const tr = document.createElement('tr');
    tr.textContent = 'You should rate more movies to get recommendations. And remember: the more movies you rate, the more accurate the recommendations you get!';
    if (myRatings.size >= RECOMMENDATIONS_MIN_RATINGS) {
        populateRecommendationsTable();
    } else {
        clearTable(tableId, tr);
    }
}

function handleSearchLabel() {
    const tr = document.createElement('tr');
    tr.textContent = 'The search results will be shown in this section.';
    clearTable('tbl-previews', tr);
}

async function populateRecommendationsTable() {
    const tableId = 'id-recommendations-tbl';
    const rateBtn = document.getElementById('id-rate-btn');
    rateBtn.disabled = true;
    const rateIpt = document.getElementById("id-rating-input");
    rateIpt.disabled = true;
    const recommendBtn = document.getElementById("id-recommend-btn");
    recommendBtn.disabled = true;
    let loadingTxt = document.createElement('tr');
    loadingTxt.textContent = 'Please stand by while we calculate your recommendations...';
    clearTable(tableId, loadingTxt);

    let myRatings = getMyRatings();
    const movies = [];
    for (let [movieId, rating] of myRatings) {
        movies.push(movieId);
    }
    const body = {'movieList': movies};
    let end, start = performance.now();
    let recommendedMovies = await callAPI('post', '/ratings', JSON.stringify(body))
        .then(resp => {
            end = performance.now();
            console.log(`Time to get response = ${end - start}ms`);
            return resp;
        })
        .then(resp => JSON.parse(resp))
        .then(movieRatings => findCorrelatedUsers(movieRatings))
        .then(correlatedUsers => findRecommendedMovies(correlatedUsers))
        .then(rm => new Set(Array.from(rm.keys()).filter(movieId => !movies.includes(movieId))));
    // console.log(`Found ${recommendedMovies.size} movies to recommend`);
    const movieObjects = [];
    let counter = 1;
    for (let movieId of recommendedMovies) {
        if (counter > 10) {
            break;
        }
        let movie = await Movie.ofMovieId(movieId);
        movieObjects.push(movie);
        counter++;
    }

    if (movieObjects && movieObjects.length > 0) {
        renderMovieTable(movieObjects, tableId);
    } else {
        loadingTxt.textContent = 'Please rate more movies to improve your chances of getting accurate recommendations.';
        clearTable(tableId, loadingTxt);
    }
    rateBtn.disabled = false;
    rateIpt.disabled = false;
    recommendBtn.disabled = false;
}

function clearTable(tableId, defaultContent) {
    const table = document.getElementById(tableId);
    let childCount = table.childNodes.length;
    for (let i = childCount - 1; i >= 0; i--) {
        table.removeChild(table.childNodes[i]);
    }
    if (defaultContent) {
        table.appendChild(defaultContent);
    }
}

async function findRecommendedMovies(userArray) {
    if (!userArray || userArray.size === 0) {
        return [];
    }
    const movies = new Map();
    for (const userId of userArray) {
        await callAPI('get', `/ratings/${userId}`)
            .then(resp => JSON.parse(resp))
            .then(ratings => {
                for (const mr of ratings) {
                    if (movies.has(mr.movieId)) {
                        if (movies.get(mr.movieId) < mr.rating) {
                            movies.set(mr.movieId, mr.rating);
                        }
                    } else {
                        movies.set(mr.movieId, mr.rating);
                    }
                }
            })
            .catch(exception => console.log(`No movie ratings found for user ${userId}`));
    }
    return new Map([...movies.entries()].sort((a, b) => b[1] - a[1]));
}

/**
 * Helper method to invoke REST APIs.
 *
 * @param method HTTP method, like 'get' or 'post'
 * @param path   path to resource, like '/movies'
 * @param data   request body (use JSON#stringify)
 * @returns {Promise<unknown>} a promise for a response
 */
async function callAPI(method, path, data) {
    let promise = new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open(method, `${backend}${path}`);
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(xhr.response);
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            }
        };
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(data);
    });

    return promise;
}


/*******************************************************/
/**************** pearson correlation ******************/
/*******************************************************/
/**
 * Calculate Pearson's Correlation/Coefficient number between 2 maps.
 * The given maps must be structured like this:
 *
 *  `Map<userId:string, Map<movieId:string, rating:number>>`
 *
 * @param user1
 * @param user2
 * @returns {number}
 */
function calculatePearson(user1, user2) {
    // console.log(user1);
    // console.log(user2);
    var existu1u2 = {};
    for (const [movieId, rating] of user1) {
        const user2Movies = Array.from(user2.keys());
        if (user2Movies.includes(movieId)) {
            existu1u2[movieId] = 1
        }
    }
    var num_existence = len(existu1u2);
    if (num_existence == 0) {
        return 0;
    }
    //store the sum and the square sum of both p1 and p2
    //store the product of both
    var u1_sum = 0,
        u2_sum = 0,
        u1_sq_sum = 0,
        u2_sq_sum = 0,
        prod_u1u2 = 0;
    //calculate the sum and square sum of each data point
    //and also the product of both point
    for (var movieId in existu1u2) {
        const user1Rating = user1.get(parseInt(movieId)) instanceof Number ? user1.get(parseInt(movieId)) : parseFloat(user1.get(parseInt(movieId)));
        const user2Rating = user2.get(parseInt(movieId));
        u1_sum += user1Rating;
        u2_sum += user2Rating;
        u1_sq_sum += Math.pow(user1Rating, 2);
        u2_sq_sum += Math.pow(user2Rating, 2);
        prod_u1u2 += user1Rating * user2Rating;
    }
    var numerator = prod_u1u2 - (u1_sum * u2_sum / num_existence);
    var st1 = u1_sq_sum - Math.pow(u1_sum, 2) / num_existence;
    var st2 = u2_sq_sum - Math.pow(u2_sum, 2) / num_existence;
    var denominator = Math.sqrt(st1 * st2);
    if (denominator == 0) {
        return 0;
    } else {
        return numerator / denominator;
    }
}

function len(obj) {
    let len = 0;
    for (let i in obj) {
        len++
    }
    return len;
}

class Movie {
    movieId;
    title;
    genres;
    release;

    constructor(movieId, title, genres, release) {
        this.movieId = movieId;
        this.title = title;
        this.genres = genres;
        this.release = release;
    }

    static async ofMovieId(movieId) {
        this.movieId = movieId;
        return callAPI('get', `/movie/${movieId}`)
            .then(resp => JSON.parse(resp))
            .then(resp => resp[0])
            .then(m => new Movie(movieId, m.title, m.genres));
    }
}